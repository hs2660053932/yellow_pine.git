//移动字符,并固定字符 
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<windows.h>

int main()
{
	int x=1;//列 
	int y=1;//行
	char input_char;
	
	int nx,ny;//要固定字符的列，行
	scanf("%d %d",&nx,&ny); 
	 
	int i;
	
	while(1)
	{
		CONSOLE_CURSOR_INFO cursor_info = {1, 0};  // 第二个值为0表示隐藏光标
		SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursor_info);
		if(y==0)	y=1;
		if(x==0)	x=1;
		system("cls");   // 清屏函数
		
		if(y>ny)//移动点 在下面 
		{
			for(i=1;i<ny;i++)
			{
				printf("\n");
			}
			for(i=1;i<nx;i++)
			{
				printf(" ");
			}
			printf("@\n");
			
			for(i=1;i<y-ny;i++)
			{
				printf("\n");
			}
			for(i=1;i<x;i++)
			{
				printf(" ");
			}
			printf("*");
		}
		else if(y==ny)//同一行 
		{
			for(i=1;i<y;i++)
			{
				printf("\n");
			}
			if(x>nx)//移动点右边 
			{
				for(i=1;i<nx;i++)
					printf(" ");
				printf("@");
				for(i=1;i<x-nx;i++)
					printf(" ");
				printf("*");
			}
			else if(x<nx)//移动点左边 
			{
				for(i=1;i<x;i++)
					printf(" ");
				printf("*");
				for(i=1;i<nx-x;i++)
					printf(" ");
				printf("@");
			}
			else if(x==nx)//同一点 
			{
				for(i=1;i<x;i++)
					printf(" ");
				printf("!");
			 }
		}
		else//移动点在上面 
		{
			for(i=1;i<y;i++)
			{
				printf("\n");
			}
			for(i=1;i<x;i++)
			{
				printf(" ");
			}
			printf("*\n");
			
			for(i=1;i<ny-y;i++)
			{
				printf("\n");
			}
			for(i=1;i<nx;i++)
			{
				printf(" ");
			}
			printf("@");
		}
		
		if(kbhit())
		{
			input_char=getch();
			if(input_char=='w')
				y--;  // 位置上移
			if (input_char== 's')
				y++;  // 位置下移
			if (input_char== 'a')
				x--;  // 位置左移
			if (input_char== 'd')
				x++;  // 位置右移 
		}
	}
	return 0; 
 } 
